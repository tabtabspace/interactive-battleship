__author__ = 'brinschw'

"""
Created on Sun Jun 11 17:28:35 2017

@author: kurtb_000
"""

import unittest
import pandas as pd


def read_fleet_file(fname):
    """
    read the fleet definition file
    :param fname: filename of csc file
    :return: df : csv table
    """
    try:
        df = pd.read_csv(fname, header=None, usecols=[0, 1, 2])
        df[0] = df[0].astype(str)  # cast name to string
        df[1] = df[1].astype(int)  # cast number to int
        df[2] = df[2].fillna(-1).astype(int)
        df.name = 'fleet'
        return df
    except:
        return ValueError('Invalid Fleet file : check format\n' +
                          'three columns should be type STR, INT, INT')


def read_move_file(fname):
    """
    read the df definition file
    :param fname: filename of csv file
    :return: df : csv table
    """
    try:
        df = pd.read_csv(fname, header=None, usecols=[0, 1])
        df[0] = df[0].fillna(-1).astype(int)
        df[0] = df[0].fillna(-1).astype(int)
        df.name = 'move'
        return df
    except:
        return ValueError('Invalid Move file : check format\n' +
                          'Two columns should be type INT, INT')


def read_placement_file(fname):
    """
    read the move definition file
    :param fname: filename of csv file
    :return: df : csv table
    """
    try:
        df = pd.read_csv(fname, header=None, usecols=[0, 1, 2, 3])
        df[0] = df[0].fillna(-1).astype(int)
        df[1] = df[1].fillna(-1).astype(int)
        df[2] = df[2].astype(str)
        df[3] = df[3].astype(str)
        df.name = 'place'
        return df
    except:
        return ValueError('Invalid Place file : check format\n' +
                          'Four columns should be type STR, STR, INT, INT')


class TestUM(unittest.TestCase):



    def setUp(self):

        pass

    def test_missing_data(df):
        """
        tests if any of the fields in df has missing data
        Missing data (string fields) have value 'nan',
        Missing data (int fields) have value -1
        :param df: input table
        :return:
        """
        has_missing_data = any(df == -1) | (df.values.any() == 'nan')
        assert(not has_missing_data, \
               'one or more fields in ' + df.name +
               ' is missing data, please check')


    def test_strings_a_3(self):
        self.assertEqual(multiply('a', 3), 'aaa')

    def test_strings_a_3(self):
        self.assertEqual(multiply('a', 3), 'aaa')




if __name__ == '__main__':
    unittest.main()