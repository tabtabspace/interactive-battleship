__author__ = 'brinschw'

"""
Created on Sun Jun 11 17:28:35 2017

@author: kurtb_000
"""

import pandas as pd

# globals
MAX_SHIPS = 10
BOARD_LENGTH = 10


def read_fleet(fname):
    """     read fleet csv file    """
    try:
        fleet = pd.read_csv(fname, header=None)
        fleet.index += 1  # 1 based indexing
        fleet.rename(columns={0: 'TYPE', 1: 'NUMBER', 2: 'LENGTH'},
                     inplace=True)
        return fleet
    except Exception as e:
        print(type(e))  # the exception instance
        print(e.args)  # arguments stored in .args
        print(e)  # __str__ allows args to printed directly
        print('Invalid Fleet file : check format\n' +
              'three columns should be type STR, INT, INT')


def read_moves(fname):
    """     read moves csv file     """

    try:
        moves = pd.read_csv(fname, header=None)
        moves.index += 1
        moves.rename(columns={0: 'X', 1: 'Y'}, inplace=True)
        return moves
    except Exception as e:
        print(type(e))  # the exception instance
        print(e.args)  # arguments stored in .args
        print(e)  # __str__ allows args to printed directly
        print('Invalid moves file: check format\n' +
              'two columns should be type INT, INT')


def read_place(fname):
    """ read the move definition file """
    try:
        place = pd.read_csv(fname, header=None)
        place.index += 1
        place.rename(columns={0: 'X', 1: 'Y', 2: 'TYPE', 3: 'ORIENT'},
                     inplace=True)
        return place
    except Exception as e:
        print(type(e))  # the exception instance
        print(e.args)  # arguments stored in .args
        print(e)  # __str__ allows args to printed directly
        return 'Invalid Place file : check format\n' + \
               'Four columns should be type INT, INT, STR, STR'


def has_no_missing_values(df):
    return df.notnull().values.all()


def has_duplicates(df):
    return df.duplicated().any()


def is_within_range(df, lo, hi):
    """returns boolean if all values in df within lo and hi (inclusive) """
    return ((df >= lo) & (df <= hi)).values.all()


def get_end_place(place, fleet):
    """returns updated place df with end coordinates"""

    place = place.copy().merge(fleet[['TYPE', 'LENGTH']])
    place['XE'] = 0  # add end coordinate X
    place['YE'] = 0  # add end coordinate Y
    place['WITHIN'] = False  # Boolean = True if placement is within board

    # calculate end coordinates for horizontal placement
    idx_H = place.ORIENT == 'H'
    place.loc[idx_H, 'XE'] = \
        place.loc[idx_H, 'X'] + place.loc[idx_H, 'LENGTH'] - 1
    place.loc[idx_H, 'YE'] = place.loc[idx_H, 'Y']

    # calculate end coordinates for vertical placement
    idx_V = ~idx_H
    place.loc[idx_V, 'XE'] = place.loc[idx_V, 'X']
    place.loc[idx_V, 'YE'] = \
        place.loc[idx_V, 'Y'] + place.loc[idx_V, 'LENGTH'] - 1

    place.WITHIN = (place.XE <= BOARD_LENGTH) & (place.YE <= BOARD_LENGTH)
    return place


def find_unknown_ship_types_in_place(place, fleet):
    """check all placement ship types already defined in fleet
    if set is empty then no unaccounted for """
    return set(place.TYPE) - set(fleet.TYPE)


def is_fleet_feasible(fleet):
    """
    read the fleet definition file
    :param fleet: dataframe holding the fleetcsv input file
    :return: (ok, errs)
        ok = Boolean
        errs = list of error messages (empty if ok=True)
    """

    errs = []

    no_mv = has_no_missing_values(fleet[['TYPE', 'NUMBER', 'LENGTH']])
    if not no_mv:
        errs.append("ERROR FLEET FILE: Missing Values")

    no_longs = is_within_range(fleet.LENGTH.astype(int), 1, BOARD_LENGTH)
    if not no_longs:
        errs.append(
            "ERROR FLEET FILE: INVALID SHIP LENGTHS , must be integer " +
            "in range 1 to BOARD_LENGTH=" + str(BOARD_LENGTH))

    no_bad_num = is_within_range(fleet.NUMBER.astype(int), 1, MAX_SHIPS)
    if not no_bad_num:
        errs.append("ERROR FLEET FILE: Invalid Ship numbers, " +
                    "must be integer in range 1 ... MAX_SHIPS=" +
                    str(MAX_SHIPS))

    dups = has_duplicates(fleet.TYPE)
    if dups:
        errs.append("ERROR FLEET FILE: Duplicate Ship Types Found")

    fleet_area = sum(fleet.NUMBER.astype(int) * fleet.LENGTH.astype(int))
    no_overcrowd = fleet_area <= BOARD_LENGTH * BOARD_LENGTH
    if not no_overcrowd:
        errs.append("ERROR FLEET FILE: Total Fleet area = " + str(fleet_area) +
                    " exceeds board area = " + str(BOARD_LENGTH * BOARD_LENGTH))
    return (not errs, errs)


def is_moves_feasible(moves):
    """
    check all moves are valid
    :param moves: dataframe holding the moves csv input file
    :return: (ok, errs)
        ok = Boolean
        errs = list of error messages (empty if ok=True)
    """

    errs = []

    no_mv = has_no_missing_values(moves[['X', 'Y']])
    if not no_mv:
        errs.append("ERROR MOVES FILE: Missing Values")

    no_bad_coords = is_within_range(
        moves[['X', 'Y']].astype(int), 1, BOARD_LENGTH)
    if not no_bad_coords:
        errs.append("ERROR MOVES FILE: cell values must be integer " +
                    "in range 1 ... MAX_SHIPS=" + str(BOARD_LENGTH))
    return (not errs, errs)


def is_all_fleet_placed(place, fleet):
    """ check all ships in fleet have been placed """
    sum_place_by_type = \
        list(place.sort_values('TYPE').groupby(place.TYPE).size())
    fleet_tots = list(fleet.sort_values('TYPE').NUMBER)
    return sum_place_by_type == fleet_tots


def is_place_feasible(place, fleet):
    """
    check all placements are valid
    :param place: dataframe holding the moves csv input file
    :return: (ok, errs)
        ok = Boolean
        errs = list of error messages (empty if ok=True)
    """
    errs = []

    no_mv = has_no_missing_values(place[['X', 'Y', 'TYPE', 'ORIENT']])
    if not no_mv:
        errs.extend("ERROR PLACE FILE: Missing Values")

    no_bad_coords = is_within_range(
        place[['X', 'Y']].astype(int), 1, BOARD_LENGTH)
    if not no_bad_coords:
        errs.append("ERROR PLACE FILE: cell values must be integer " +
                    "in range 1 ... MAX_SHIPS=" + str(BOARD_LENGTH))

    unknown_ship_types = find_unknown_ship_types_in_place(place, fleet)
    if unknown_ship_types:
        errs.append("ERROR PLACE FILE: Contains ship types not " +
                    "specified in fleet file")

    no_bad_orient = place.ORIENT.str.upper().isin(['H', 'V']).all()
    if not no_bad_orient:
        errs.append("\nERROR PLACE FILE: Orientations must be H or V only")

    place = get_end_place(place, fleet)
    no_overextend = place.WITHIN.all()
    if not no_overextend:
        errs.append("ERROR PLACE FILE: Following placings overextend board:\n"
                    + place.loc[~place.WITHIN].to_csv())

    all_fleet_placed = is_all_fleet_placed(place, fleet)
    if not all_fleet_placed:
        errs.append("ERROR PLACE FILE: Mismatch between place and fleet " +
                    "files:\nCheck sums of each ship type agree")
    return (not errs, errs)


if __name__ == '__main__':
    # unittest.main()
    pass
