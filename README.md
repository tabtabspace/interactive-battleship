# README #

Hi there


### What is this repository for? ###

* This repo is for the Interactive Battleship program for the SWEng course by team TabTabSpace
* The non-interatice Battleship code was inherited from team Frankenspillschwitz
* Version - Release-ish
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Run file: RunBattleship.py
* Input files: current input file path set to "testfiles/bluewin/"

## User Guide ##

### Battleship Input files ###

* The fleet input files is called by the initialise_fleet() method in main
	+ a fleet file is required for each player, these much be named
		- fleetplayer1.txt
		- fleetplayer2.txt
* The ship placement input file is called by the initialise_placement() method in main
	+ a placement file is required for each player, these much be named
		- placementplayer1.txt
		- placementplayer2.txt
* The player moves are input from the command line during game  


### To run the program the user should: ###

1. Run �RunBattleship.py�
1. Enter an integer for the column and row to shoot at
1. A plot will open showing the hit/miss/unknown grid
1. Click the figure to close it
1. Repeat steps 2-4 until victory

### Battleship Output ###

Each players shots are displayed in a separate colour plot

Colourmap key

* Green = unknown
* Blue = Miss
* White = Hit

## Testing ##

* Unit tests developed by Team Frankenspillschwitz.
* These have not been updated or added to.


### Who do I talk to? ###

* Product Manager - Marc West
* Python Guru - Tom Keevers
* Steve Rowling
* Rae Hall
