__author__ = 'brinschw'

"""
Created on Sun Jun 11 17:28:35 2017
@author: kurtb_000
"""

import unittest
from checkfiles import *

if __name__ == '__main__':
    #unittest.main()
    PATH = r"H:\Work\Battleships\testfiles\Fail1 Missing Data\\"
    fname_fleet = PATH + 'Fleet file.txt'

    fleet = read_fleet(fname_fleet)

    assert has_no_missing_values(fleet[['TYPE', 'NUMBER', 'LENGTH']]), \
        "ERROR FLEET FILE: Missing Values"
