import pandas as pd
import numpy as np
import matplotlib.pyplot
import time

class main():

    def __init__(self):
        self.num_players = 2
        self.player_list = []
        #read in fleet file
        self.initialise_fleet()
        #read in placement file
        self.initialise_placement()
        #read in move file
        self.play_game()

    def initialise_fleet(self):
        for i in range(self.num_players):
            name = "player" + str(i+1)
            player = Player(name)
            self.player_list.append(player)
            fleet_info = self.read_fleet_file("testfiles/bluewin/fleet"+player.name+".txt")
            for index, row in fleet_info.iterrows():
                #for each row in fleet file, add ships to the player
                player.add_ships(row[0],row[1],row[2])
    
    def initialise_placement(self):   
        for player in self.player_list:
            fleet_placement = self.read_placement_file("testfiles/bluewin/placement"+player.name+".txt")
            for index, row in fleet_placement.iterrows():
                #for each row in placement file, update the ships with their placement on the board
                player.update_ships(row[0],row[1],row[2],row[3])
    
    def play_game(self):       
        opponent = self.player_list[0]
        me = self.player_list[1]
        cont = False
        turn = 0
        move = [0,0]
        while not cont:
            if turn%2 == 0:
                opponent = self.player_list[1]
                me = self.player_list[0]
            else:
                opponent = self.player_list[0]
                me = self.player_list[1]
            valid = False
            while not valid:
                A = input(me.name+" enter column:  ")
                if A == "":
                    print("Please enter an integer.")
                else:
                    try:
                        move[0] = int(A)
                        if move[0] <= me.board.width and move[0] >= 1:
                            valid = True
                        else:
                            print("Off the board, input must be from 1 to "+str(me.board.width))
                    except:
                        print("Please enter an integer.")
            
            valid = False
            while not valid:
                B = input(me.name+" enter row:  ")
                if B == "":
                    print("Please enter an integer.")
                else:
                    try:
                        move[1] = int(B)
                        if move[1] <= me.board.height and move[1] >= 1:
                            valid = True
                        else:
                            print("Off the board, input must be from 1 to "+str(me.board.height))
                    except:
                        print("Please enter an integer.")
            #matplotlib.pyplot.close()
            print(me.name+" is firing a shot at "+str(move[0])+","+str(move[1]))
            #for each row in move file, fire the shots at the opposing players board
            cont = opponent.fire_shot(move[1],move[0])
            fig = matplotlib.pyplot.figure()
            ax1=fig.add_subplot(2,1,1)
            ax2=fig.add_subplot(2,1,2)
            opponent.plot_information(turn%2, ax1, ax2)
            me.plot_information((turn+1)%2, ax1, ax2)
            matplotlib.pyplot.waitforbuttonpress()
            matplotlib.pyplot.close()
            if cont:
                print("GG")
                break
            turn = turn + 1
        
    def read_fleet_file(self,fname):
        """
        read the fleet definition file
        :param fname: filename of csc file
        :return: df : csv table
        """
        try:
            df = pd.read_csv(fname, header=None, usecols=[0, 1, 2])
            df[0] = df[0].astype(str)  # cast name to string
            df[1] = df[1].astype(int)  # cast number to int
            df[2] = df[2].fillna(-1).astype(int)
            df.name = 'fleet'
            return df
        except:
            return ValueError('Invalid Fleet file : check format\n' +
                          'three columns should be type STR, INT, INT')
        
    def read_move_file(self,fname):
        """
        read the df definition file
        :param fname: filename of csv file
        :return: df : csv table
        """
        try:
            df = pd.read_csv(fname, header=None, usecols=[0, 1])
            df[0] = df[0].fillna(-1).astype(int)
            df[0] = df[0].fillna(-1).astype(int)
            df.name = 'move'
            return df
        except:
            return ValueError('Invalid Move file : check format\n' +
                          'Two columns should be type INT, INT')                    

    def read_placement_file(self,fname):
        """
        read the move definition file
        :param fname: filename of csv file
        :return: df : csv table
        """
        try:
            df = pd.read_csv(fname, header=None, usecols=[0, 1, 2, 3])
            df[0] = df[0].fillna(-1).astype(int)
            df[1] = df[1].fillna(-1).astype(int)
            df[2] = df[2].astype(str)
            df[3] = df[3].astype(str)
            df.name = 'place'
            return df
        except:
            return ValueError('Invalid Place file : check format\n' +
                          'Four columns should be type STR, STR, INT, INT')
    
    
class Player(object):
    def __init__(self, name):
        self.name = name
        print("Creating player "+self.name)
        self.ship_list = []
        self.ship_index = 0
        self.destroy_count = 0
        self.board = Board(name)
        # keevers update
        self.information = -np.ones((10,10))
        
    def plot_information(self, player, ax1, ax2):
        normalize = matplotlib.colors.Normalize(vmin=-1.,vmax=1.)
        #fig, ax = matplotlib.pyplot.subplots(nrows=2, ncols=1)
        if player == 0:
            cmaptype = 'ocean' 
            ax_cur = ax1
        else:
            #cmaptype = 'jet'
            cmaptype = 'ocean'            
            ax_cur = ax2
        matplotlib.pyplot.sca(ax_cur)
        matplotlib.pyplot.imshow(self.information,cmap=cmaptype, norm=normalize, interpolation='none')
        matplotlib.pyplot.show()     
        
    def add_ships(self,ship_name,ship_num,ship_length):
        #create new ships
        #destroy count tracks the num of ships remaining for the player
        while ship_num > 0:
            self.ship_list.append(Ship(self.ship_index+1,ship_name,ship_length,self.name))
            ship_num -= 1
            self.ship_index += 1
            self.destroy_count += 1
    
    def update_ships(self,x,y,ship_name,placement):
        for ship in self.ship_list:
            #go through the ships
            if ship.name == ship_name and ship.x == -1:
                print("Updating ship")
                #get the ship where the name is the same but the coords have not been set
                #break out of the for-loop once positions are set
                ship.x = x-1
                ship.y = y-1
                ship.placement = placement
                #place the ship ont he board
                self.board.place_ship(ship)
                break
    
    def fire_shot(self,x,y):
        win_condition = False
        #check whats on the board at that spot
        report = self.board.report_shot(x-1,y-1)
        if report == 0:
            print("Your shot hit Empty Ocean")
            self.information[x-1,y-1] = 0
        if report == -1:
            print("You have already fired on this position")
        if report > 0:
            print("Your shot hit a ship")
            self.information[x-1,y-1] = 1
            #cycle through the ship list for the ship with that ID
            for ship in self.ship_list:
                if ship.ID == report:
                    #reduce the ship's hit counter
                    ship.hit_count -= 1
                    #if this counter reaches 0 then the ships has been destroyed
                    if ship.hit_count == 0:
                        print(ship.name+" destroyed")
                        self.destroy_count -= 1
                        #check if the player has any ships left
                        if self.destroy_count==0:
                            #print(self.name+" has won!!!")
                            print(self.name+" is a loser!!!")
                            #if no ships left set the win condition
                            win_condition = True
                            return win_condition
        return win_condition


class Board(object):
    def __init__(self, owner):
        self.owner = owner
        print("Creating board for "+self.owner)
        self.width = 10
        self.height = 10
        #create a set of 10x10 tiles set to 0
        self.tiles = [[0 for x in range(self.width)] for y in range(self.height)]
        
    def place_ship(self,ship):
        #put the ship ID into the tiles where the ship is
        newx = ship.x+1
        newy = ship.y+1
        if ship.placement == "H":
            newx = ship.x + ship.length
        if ship.placement == "V":
            newy = ship.y + ship.length
        for i in range(ship.x,newx):
            for j in range(ship.y,newy):
                self.tiles[j][i] = ship.ID
        print("Placing ship on board")
        #print(self.tiles)
    
    def report_shot(self,x,y):
        shipID = self.tiles[x][y]
        #print(str(x)+str(y)+str(self.tiles[2][3]))
        #print(shipID)
        self.tiles[x][y] = -1
        return shipID        

class Ship(object):
    def __init__(self, ID, name, length, owner):
        self.ID = ID
        self.length = length
        self.name = name
        self.owner = owner
        self.x = -1
        self.y = -1
        self.placement = ""
        self.hit_count = length
        print("Creating ship for "+self.owner)    

main()
